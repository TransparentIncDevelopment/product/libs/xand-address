# xand-address

Entities participating on a blockchain network are each identified by an address, which is deterministically derived from the public part of the their keypair. When an entity signs a transaction with their corresponding private key, other entities on the network can verify it.

This crate was originally part of [`thermite`](https://gitlab.com/TransparentIncDevelopment/product/apps/thermite), but has been extracted so other crates can use it.

## Crate Publishing

1. In a feature branch, bump the crate version in [`Cargo.toml`](./Cargo.toml)
1. When feature branch is ready, run the `publish-beta` job and verify the crate is published as expected
1. Get necessary approvals, merge, and verify the `publish` job successfully publishes the non-beta version of the crate
