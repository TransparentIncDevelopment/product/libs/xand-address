//! An address is derived from the public portion of a network entity's keypair.
//! They are commonly used on a blockchain to convey ownership of assets or
//! the issuer of a transaction.
//!
//! Currently, this means an SS58-check address as specified by substrate here:
//! https://github.com/paritytech/substrate/wiki/External-Address-Format-(SS58)

use blake2::{Blake2b, Digest};
use serde::{Deserialize, Serialize};
use std::convert::{TryFrom, TryInto};
use std::fmt::{Display, Error, Formatter};
use std::str::FromStr;
use thiserror::Error;

#[derive(Clone, Debug, Hash, Eq, PartialEq, Serialize)]
pub struct Address(String);

impl Display for Address {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result<(), Error> {
        Display::fmt(&self.0, f)
    }
}

impl TryFrom<String> for Address {
    type Error = AddressError;

    fn try_from(value: String) -> Result<Self, Self::Error> {
        let bytes = bs58::decode(&value).into_vec()?;
        if bytes.len() != 35 {
            return Err(AddressError::InvalidSs58Length { len: bytes.len() });
        }
        let hash = ss58hash(&bytes[..33]);
        if bytes[33..] != hash.as_ref()[0..2] {
            return Err(AddressError::InvalidSs58Checksum);
        }
        Ok(Address(value))
    }
}

impl FromStr for Address {
    type Err = AddressError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        s.to_owned().try_into()
    }
}

impl<'de> Deserialize<'de> for Address {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: serde::Deserializer<'de>,
    {
        Address::try_from(String::deserialize(deserializer)?).map_err(serde::de::Error::custom)
    }
}

const PREFIX: &[u8] = b"SS58PRE";
/// Hashes a byte slice using blake2 in accordance with the ss58 spec
/// https://github.com/paritytech/substrate/wiki/External-Address-Format-(SS58)
///
/// This is re-implemented to enable usage without depending on substrate
fn ss58hash(data: &[u8]) -> impl AsRef<[u8]> {
    let mut context = Blake2b::new();
    context.input(PREFIX);
    context.input(data);
    context.result()
}

#[derive(Clone, Debug, Eq, Error, PartialEq, Serialize)]
pub enum AddressError {
    #[error("Address did not have a valid checksum")]
    InvalidSs58Checksum,
    #[error("Expected 35 bytes in the address, but found {len}")]
    InvalidSs58Length { len: usize },
    #[error("Address wasn't proper b58: {source:?}")]
    NotBase58 {
        #[serde(serialize_with = "xand_utils::snafu_extensions::debug_serialize")]
        #[from]
        source: bs58::decode::Error,
    },
}

#[cfg(test)]
mod test {
    use super::*;
    use proptest::proptest;
    use sp_core::{crypto::Ss58Codec, ed25519, sr25519, Pair};

    proptest! {
        #[test]
        fn test_address_validation(pubkey_bytes: [u8; 32]) {
            let sr_key = sr25519::Public::from_raw(pubkey_bytes);
            let ed_key = ed25519::Public::from_raw(pubkey_bytes);
            let sr_str = sr_key.to_ss58check();
            let ed_str = ed_key.to_ss58check();
            // Both of these should never fail since they are produced *by* the substrate lib.
            Address::try_from(sr_str).unwrap();
            Address::try_from(ed_str).unwrap();
        }
    }

    #[test]
    fn test_bad_address_checksum_fails_validation() {
        let sr_key = sr25519::Pair::from_seed(&[42; 32]);
        let mut as_ss58 = sr_key.public().to_ss58check();
        // Mess with checksum bytes
        as_ss58.pop();
        as_ss58.push('s');
        assert_eq!(
            as_ss58.parse::<Address>().unwrap_err(),
            AddressError::InvalidSs58Checksum
        );
    }

    #[test]
    fn test_bad_address_length_fails_validation() {
        let sr_key = sr25519::Pair::from_seed(&[42; 32]);
        let mut as_ss58 = sr_key.public().to_ss58check();
        // Drop a char
        as_ss58.pop();
        assert_eq!(
            Address::try_from(as_ss58.clone()).unwrap_err(),
            AddressError::InvalidSs58Length { len: 34 }
        );
        // Make it too long
        as_ss58.push('s');
        as_ss58.push('s');
        assert_eq!(
            Address::try_from(as_ss58).unwrap_err(),
            AddressError::InvalidSs58Length { len: 36 }
        );
    }

    #[test]
    fn deserializing_invalid_address_fails() {
        let address = serde_json::from_str::<Address>(r#""""#);
        assert!(address.is_err());
    }
}
